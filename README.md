[![SCUI Version](https://img.shields.io/badge/SCUI_Admin-1.5.0-green?maxAge=2592000&color=blue)](https://gitee.com/lolicode/scui)

[![Vue Version](https://img.shields.io/badge/vue.js-3.x-green)](https://v3.vuejs.org/)
[![Element-Plus Version](https://img.shields.io/badge/element--plus-latest-blue)](https://element-plus.gitee.io/zh-CN/)

项目简述
----

基于 [SCUI Admin](https://lolicode.gitee.io/scui-doc/) 实现的 [DataCMF V0.0.1-Admin端]

环境和依赖
----

- node
- yarn
- webpack
- eslint
- @vue/cli ~3
- vue 3.x
- Element-Plus
- [SCUI Admin](https://gitee.com/lolicode/scui) - SCUI Admin 实现

> 请注意，我们强烈建议本项目使用 [Yarn](https://yarnpkg.com/) 包管理工具，这样可以与本项目演示站所加载完全相同的依赖版本 (yarn.lock) 。由于我们没有对依赖进行强制的版本控制，采用非 yarn 包管理进行引入时，可能由于所依赖的库已经升级版本而引入了新版本所导致的问题。作者可能会由于时间问题无法及时排查而导致您采用本项目作为基项目而出现问题。



项目运行和发布
----

- 修改配置文件
```
1. 打开文件：/public/config.js 
2. 修改服务端API信息
```

- 安装依赖
```
yarn install
```

- 开发模式运行
```
yarn run serve
```

- 编译项目
```
yarn run build
```

- 发布项目
```
将编译完成的所有文件，复制替换到DataCMF后端项目目录下
```
