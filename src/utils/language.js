import { supportLang } from '@/locales'

const language = {};

language.viewSupportLang = function(){
	let _list = [];
	for (var lang in supportLang)
	{
		_list.push({
			label: supportLang[lang]['label'],
			value: lang,
		});
	}
	return _list;
};

export default language;
