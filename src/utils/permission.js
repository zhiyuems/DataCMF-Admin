import tool from '@/utils/tool';
import { PERMISSION } from '@/config';

export function permission(data) {
	let permissions = tool.data.get("PERMISSIONS");
	if(!permissions){
		return false;
	}

	// 如果是超级管理员，直接允许
	if( rolePermission(PERMISSION.SUPER_ADMINISTRATOR) ) {
		return true;
	}
	let isHave = permissions.includes(data);
	return isHave;
}

export function rolePermission(data) {
	let userInfo = tool.data.get("USER_INFO");
	if(!userInfo){
		return false;
	}
	let role = userInfo.role;
	if(!role){
		return false;
	}
	let isHave = role.includes(data);
	return isHave;
}
