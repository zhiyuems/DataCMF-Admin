/*
 * @Descripttion: 工具集
 * @version: 1.1
 * @LastEditors: sakuya
 * @LastEditTime: 2021年7月20日10:58:41
 */

import CryptoJS from 'crypto-js';
import { API_SIGN_KEY } from '@/config';

const tool = {}

/* localStorage */
tool.data = {
	set(table, settings) {
		var _set = JSON.stringify(settings)
		return localStorage.setItem(table, _set);
	},
	get(table) {
		var data = localStorage.getItem(table);
		try {
			data = JSON.parse(data)
		} catch (err) {
			return null
		}
		return data;
	},
	remove(table) {
		return localStorage.removeItem(table);
	},
	clear() {
		return localStorage.clear();
	}
}

/*sessionStorage*/
tool.session = {
	set(table, settings) {
		var _set = JSON.stringify(settings)
		return sessionStorage.setItem(table, _set);
	},
	get(table) {
		var data = sessionStorage.getItem(table);
		try {
			data = JSON.parse(data)
		} catch (err) {
			return null
		}
		return data;
	},
	remove(table) {
		return sessionStorage.removeItem(table);
	},
	clear() {
		return sessionStorage.clear();
	}
}

/* Fullscreen */
tool.screen = function (element)
{
	var isFull = !!(document.webkitIsFullScreen || document.mozFullScreen || document.msFullscreenElement || document.fullscreenElement);
	if(isFull){
		if(document.exitFullscreen) {
			document.exitFullscreen();
		}else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		}else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		}else if (document.webkitExitFullscreen) {
			document.webkitExitFullscreen();
		}
	}else{
		if(element.requestFullscreen) {
			element.requestFullscreen();
		}else if(element.msRequestFullscreen) {
			element.msRequestFullscreen();
		}else if(element.mozRequestFullScreen) {
			element.mozRequestFullScreen();
		}else if(element.webkitRequestFullscreen) {
			element.webkitRequestFullscreen();
		}
	}
}

/* 复制对象 */
tool.objCopy = function (obj)
{
	return JSON.parse(JSON.stringify(obj));
}

/**
 * 计算对象的差集
 *
 * @param {Object} oldData 要被对比的数组
 * @param {Object} newData 比较的数组
 *
 * @return {Object}
 */
tool.objDiff = function(oldData, newData)
{
	let resultObj = {}
	Object.keys(oldData).forEach(key => {
		if(oldData[key] !== newData[key]){
			resultObj[key] = newData
		}
	});
	return resultObj // 获取差集
}

/**
 * 判断是否为索引数组
 *
 * @param {any} data 待判断数据
 *
 * @return {Boolean}
 */
tool.isIndexArray = function(data)
{
	var isIndexArray = false;
	if ( typeof data ==='object' ){
		if( data.constructor===Array ){
			isIndexArray = true;
		}
	}
	return isIndexArray;
}

/**
 * 判断是否为索引数组
 *
 * @param {any} data 待判断数据
 *
 * @return {Boolean}
 */
tool.isArrayOrObject = function(data)
{
	return typeof data === 'object';
}

/**
 * 判断是否包含中文
 *
 * @param {String} str 待判断数据
 *
 * @return {Boolean}
 */
tool.isChina = function(str)
{
	var reg=/^[\u4E00-\u9FA5]+$/;
	return !reg.test(str);
}


/**
 * 检查数组中是否存在某个值
 *
 * @param {String}       needle   待搜索的值，区分大小写
 * @param {Array|Object} haystack 待搜索的数组
 * @param {Boolean}      strict   没有设置 strict 则使用宽松的比较
 *
 * @return {Boolean}
 * @throws `needle` Arguments Error
 * @throws `haystack` Arguments Is Not Array
 */
tool.in_array = function(needle, haystack, strict)
{
	// 是否开启检查数据类型
	strict = strict?strict:false;
	if ( typeof strict !== 'boolean' ){
		throw "`strict` Arguments Is Not Boolean";
	}

	if( typeof needle!=='string' && typeof needle!=='number' ){
		throw "`needle` Arguments Error";
	}

	if( !haystack || typeof haystack!=='object' ){
		throw "`haystack` Arguments Is Not Array";
	}

	// 判断是否为索引数组
	var isIndexArray = tool.isIndexArray(haystack);

	// 检索判断
	let _in = false;
	if( isIndexArray === true ){
		_in = haystack.indexOf(needle)>=0;
	}else{
		for(var key in haystack){
			_in = strict===true?key===needle:key==needle;
			if( _in ){break;}
		}
	}

	return _in;
}

/* 日期格式化 */
tool.dateFormat = function (date, fmt='yyyy-MM-dd hh:mm:ss')
{
	date = new Date(date)
	var o = {
		"M+" : date.getMonth()+1,                 //月份
		"d+" : date.getDate(),                    //日
		"h+" : date.getHours(),                   //小时
		"m+" : date.getMinutes(),                 //分
		"s+" : date.getSeconds(),                 //秒
		"q+" : Math.floor((date.getMonth()+3)/3), //季度
		"S"  : date.getMilliseconds()             //毫秒
	};
	if(/(y+)/.test(fmt)) {
		fmt=fmt.replace(RegExp.$1, (date.getFullYear()+"").substr(4 - RegExp.$1.length));
	}
	for(var k in o) {
		if(new RegExp("("+ k +")").test(fmt)){
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
		}
	}
	return fmt;
}

/* 千分符 */
tool.groupSeparator = function (num)
{
	num = num + '';
	if(!num.includes('.')){
		num += '.'
	}
	return num.replace(/(\d)(?=(\d{3})+\.)/g, function ($0, $1) {
		return $1 + ',';
	}).replace(/\.$/, '');
}

/* 常用加解密 */
tool.crypto = {
	//MD5加密
	MD5(data){
		return CryptoJS.MD5(data).toString()
	},
	//BASE64加解密
	BASE64: {
		encrypt(data){
			return CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(data))
		},
		decrypt(cipher){
			return CryptoJS.enc.Base64.parse(cipher).toString(CryptoJS.enc.Utf8)
		}
	},
	//AES加解密
	AES: {
		encrypt(data, secretKey){
			const result = CryptoJS.AES.encrypt(data, CryptoJS.enc.Utf8.parse(secretKey), {
				mode: CryptoJS.mode.ECB,
				padding: CryptoJS.pad.Pkcs7
			})
			return result.toString()
		},
		decrypt(cipher, secretKey){
			const result = CryptoJS.AES.decrypt(cipher, CryptoJS.enc.Utf8.parse(secretKey), {
				mode: CryptoJS.mode.ECB,
				padding: CryptoJS.pad.Pkcs7
			})
			return CryptoJS.enc.Utf8.stringify(result);
		}
	}
}

/**
 * 判断是否是空值
 *
 * @return {Boolean}
 */
tool.isEmpty = function(obj)
{
	if(typeof obj === "undefined" || obj === null
		|| obj === '' || obj.length === 0 || (
			typeof obj === 'object' && ( JSON.stringify(obj) === '{}' || Object.keys(obj).length === 0 )
		) ){
		return true;
	}else{
		return false;
	}
}

/**
 * 判断是否是JSON数据
 *
 * @param {String|Object} str 待验证数据
 *
 * @return {Boolean}
 */
tool.isJson = function(str)
{
	if (typeof str == 'string') {
		try {
			JSON.parse(str);
			return true;
		} catch(e) {
			return false;
		}
	}else{
		return typeof str == 'object';
	}
};

/**
 * 去除数组(对象)中空数据
 * @link https://blog.csdn.net/u011159821/article/details/124262466
 * @param {Object|Array} arr
 * @return {Object|Array}
 */
tool.filterNullValue = function(arr)
{
	var _data=(typeof arr!="object")? [arr] : arr  //确保参数总是数组
	var _dataJudge = function(val){ // 统一过滤判断
		return val === null || val === '' || val === undefined || JSON.stringify(val) === "{}";
	};
	for ( let i in _data){
		if( typeof i === 'number' || (Number(i) === i) ){
			if(_dataJudge(_data[i])){_data.splice(i, 1);}
		}else{
			if(_dataJudge(_data[i]))delete _data[i];
		}
	}
	return _data;
};

/* 参数Sign */
tool.sign = {
	__KEY: API_SIGN_KEY || '',

	/**
	 * 设置生成SIGN的盐
	 *
	 * @param {String} key
	 *
	 * @return {void}
	 */
	setKey: function(key) {
		this.__KEY = key;
	},

	/**
	 * 获取SIGN生成盐
	 *
	 * @return {String}
	 */
	getKey: function() {
		return this.__KEY;
	},

	/**
	 * 对照Sign规则验证给定的值
	 *
	 * @param  {Array}   value
	 * @param  {String}  checkValue
	 *
	 * @return {Boolean}
	 */
	check: function(value, checkValue) {
		return this.make(value) === checkValue;
	},

	/**
	 * 生成给定值的SIGN
	 *
	 * @param  {Array}   value
	 *
	 * @return {String}
	 */
	make: function(value) {
		if (!tool.isJson(value) && typeof value === 'function')return '';

		//取key
		var keys = [];
		for (let k in value){
			if (k === 'sign'){continue;}
			keys.push(k);
		}

		// 排序
		keys.sort();

		// 取value
		var kv = [];
		var _tamp = '';
		for (let k of keys){
			if (tool.isEmpty(value[k]))continue;

			if( k !== 'timestamp' && k !== 'biz_content' ){
				if (tool.isArrayOrObject(value[k]) === true) {
					kv.push( k + '=' + JSON.stringify(tool.filterNullValue(value[k])) )
				}else{
					kv.push(k + '=' + value[k])
				}
			}else{
				_tamp = encodeURIComponent(value[k]);
				_tamp = _tamp.replace(/\(/,'%28');
				_tamp = _tamp.replace(/\)/,'%29');
				kv.push(k + '=' + encodeURIComponent(value[k])); //urlencode编码
			}
		}

		// 拼接
		var sign = kv.join('&');
		sign = sign.replace(/\(/,'%28');
		sign = sign.replace(/\)/,'%29') + '#' + this.getKey();
		console.log('sign:', sign)

		sign = tool.crypto.MD5(sign);
		sign = sign.toUpperCase();

		return sign;
	},
}

export default tool
