
export default {
	mounted(el, binding) {
		const selectWrap = el.querySelector('.el-scrollbar');
		console.log('el:', selectWrap)
		selectWrap.addEventListener('scroll', function () {
			console.log('this.scrollHeight:', this.scrollHeight)
			console.log('this.scrollTop:', this.scrollTop)
			console.log('this.clientHeight:', this.clientHeight)
			if (this.scrollHeight - this.scrollTop <= this.clientHeight) {
				binding.value();
			}
		});
	}
};
