import config from "@/config"
import http from "@/utils/request"

export default {
	upload: {
		url: `${config.API_URL}/bin/upload`,
		name: "文件上传",
		post: async function(data, config={}, type){
			type = type?type:'image';
			return await http.post(this.url + '/' + type, data, config);
		}
	},
	system: {
		environment: {
			url: `${config.API_URL}/bin/system-environment`,
			name: "获取系统环境信息",
			get: async function(){
				return await http.get(this.url);
			}
		}
	},
	file: {
		menu: {
			url: `${config.API_URL}/file/menu`,
			name: "获取文件分类",
			get: async function(){
				return await http.get(this.url);
			}
		},
		list: {
			url: `${config.API_URL}/file/list`,
			name: "获取文件列表",
			get: async function(params){
				return await http.get(this.url, params);
			}
		}
	}
}
