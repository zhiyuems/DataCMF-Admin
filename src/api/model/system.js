import config from "@/config"
import http from "@/utils/request"

export default {
	setting: {
		list: {
			url: `${config.API_URL}/system/setting`,
			name: "获取系统配置信息",
			get: async function(format){
				let params = {};
				if ( format && format==='group' ){
					params['format'] = format;
				}
				return await http.get(this.url, params);
			},
		},
		store: {
			url: `${config.API_URL}/system/setting`,
			name: "更新或创建系统配置信息",
			put: async function(params){
				return await http.put(this.url, params);
			},
			putBatch: async function(params){
				return await http.put(this.url+'/batch', params);
			},
		},
	},
	menu: {
		myMenus: {
			url: `${config.API_URL}/system/menu/my`,
			name: "获取我的菜单",
			get: async function(){
				return await http.get(this.url);
			}
		},
		list: {
			url: `${config.API_URL}/system/menu`,
			name: "获取菜单",
			get: async function(){
				return await http.get(this.url);
			}
		},
		create: {
			url: `${config.API_URL}/system/menu`,
			name: "创建菜单信息",
			post: async function(params){
				return await http.post(this.url, params);
			}
		},
		update: {
			url: `${config.API_URL}/system/menu`,
			name: "更新指定菜单信息",
			put: async function(id, params){
				return await http.put(this.url + '/' + id, params);
			}
		},
		delete: {
			url: `${config.API_URL}/system/menu`,
			name: "删除菜单信息",
			// 批量删除
			batch: async function(ids){
				return await http.delete(this.url + '/batch',{ids:ids});
			},
			// 删除单个
			one: async function(id){
				return await http.delete(this.url + '/' + id);
			}
		}
	},
	dic: {
		type: {
			list: {
				url: `${config.API_URL}/system/dict/type`,
				name: "获取字典类型列表",
				get: async function(){
					return await http.get(this.url);
				}
			},
			create: {
				url: `${config.API_URL}/system/dict/type`,
				name: "创建字典类型数据信息",
				post: async function(params){
					return await http.post(this.url, params);
				}
			},
			update: {
				url: `${config.API_URL}/system/dict/type`,
				name: "编辑指定字典类型信息",
				put: async function(id, params){
					return await http.put(this.url + '/' + id, params);
				}
			},
			delete: {
				url: `${config.API_URL}/system/dict/type`,
				name: "删除指定字典类型信息",
				delete: async function(id){
					return await http.delete(this.url + '/' + id);
				}
			},
		},
		details: {
			list: {
				url: `${config.API_URL}/system/dict`,
				name: "获取数据字典列表",
				get: async function(params){
					return await http.get(this.url, params);
				}
			},
			create: {
				url: `${config.API_URL}/system/dict`,
				name: "创建字典明细数据信息",
				post: async function(params){
					return await http.post(this.url, params);
				}
			},
			update: {
				url: `${config.API_URL}/system/dict`,
				name: "编辑指定字典明细信息",
				put: async function(id, params){
					return await http.put(this.url + '/' + id, params);
				}
			},
			delete: {
				url: `${config.API_URL}/system/dict`,
				name: "删除指定字典明细信息",
				delete: async function(id, params){
					return await http.delete(this.url + '/' + id, params);
				}
			},
		},
	},
	role: {
		list: {
			url: `${config.API_URL}/system/role`,
			name: "获取角色列表",
			get: async function(params){
				return await http.get(this.url, params);
			}
		},
		create: {
			url: `${config.API_URL}/system/role`,
			name: "创建角色信息",
			post: async function(params){
				return await http.post(this.url, params);
			}
		},
		update: {
			url: `${config.API_URL}/system/role`,
			name: "更新指定角色信息",
			put: async function(id, params){
				return await http.put(this.url + '/' + id, params);
			}
		},
		delete: {
			url: `${config.API_URL}/system/role`,
			name: "删除指定角色信息",
			delete: async function(id){
				return await http.delete(this.url + '/' + id);
			}
		},
		info: {
			url: `${config.API_URL}/system/role`,
			name: "获取指定角色信息",
			get: async function(id){
				return await http.get(this.url + '/' + id + '/edit');
			}
		},
	},
	user: {
		list: {
			url: `${config.API_URL}/system/admin`,
			name: "获取管理员列表",
			get: async function(params){
				return await http.get(this.url, params);
			}
		},
		create: {
			url: `${config.API_URL}/system/admin`,
			name: "创建管理员信息",
			post: async function(params){
				return await http.post(this.url, params);
			}
		},
		update: {
			url: `${config.API_URL}/system/admin`,
			name: "更新指定管理员信息",
			put: async function(id, params){
				return await http.put(this.url + '/' + id, params);
			}
		},
		delete: {
			url: `${config.API_URL}/system/admin`,
			name: "删除指定管理员信息",
			delete: async function(id){
				return await http.delete(this.url + '/' + id);
			}
		}
	},
	dept: {
		list: {
			url: `${config.API_URL}/system/dept`,
			name: "获取部门列表",
			get: async function(params){
				return await http.get(this.url, params);
			}
		},
		create: {
			url: `${config.API_URL}/system/dept`,
			name: "创建部门信息",
			post: async function(params){
				return await http.post(this.url, params);
			}
		},
		update: {
			url: `${config.API_URL}/system/dept`,
			name: "更新指定部门信息",
			put: async function(id, params){
				return await http.put(this.url + '/' + id, params);
			}
		},
		delete: {
			url: `${config.API_URL}/system/dept`,
			name: "删除指定部门信息",
			delete: async function(id){
				return await http.delete(this.url + '/' + id);
			}
		}
	},
	jobs: {
		list: {
			url: `${config.API_URL}/system/jobs`,
			name: "获取岗位列表",
			get: async function(params){
				return await http.get(this.url, params);
			}
		},
		create: {
			url: `${config.API_URL}/system/jobs`,
			name: "创建岗位信息",
			post: async function(params){
				return await http.post(this.url, params);
			}
		},
		update: {
			url: `${config.API_URL}/system/jobs`,
			name: "更新指定岗位信息",
			put: async function(id, params){
				return await http.put(this.url + '/' + id, params);
			}
		},
		delete: {
			url: `${config.API_URL}/system/jobs`,
			name: "删除指定岗位信息",
			delete: async function(id){
				return await http.delete(this.url + '/' + id);
			}
		}
	},
	log: {
		type: {
			url: `${config.API_URL}/system/logs/logstype`,
			name: "日志类型列表",
			get: async function(params){
				return await http.get(this.url, params);
			}
		},
		list: {
			url: `${config.API_URL}/system/logs/list`,
			name: "日志列表",
			get: async function(params){
				return await http.get(this.url, params);
			}
		}
	}
}
