
export const common = {
	successCode: 10000,          // 请求成功错误码
	invalidAuthTokenCode: 20001, // 重新登录错误码
	code: 'code',
	msg: 'msg',
	subCode: 'sub_code',         // 业务错误码
	subMsg: 'sub_msg',           // 业务错误说明
	data: 'data',
};

export const page = {
	pageSize: 20,
	data: 'data',
	total: 'total',

	request: {							//请求规定字段
		page: 'page',					//规定当前分页字段
		pageSize: 'pageSize',			//规定一页条数字段
		keyword: 'keyword',             //规定搜索字段
		prop: 'prop',					//规定排序字段名字段
		order: 'order'					//规定排序规格字段
	},
};

export default {
	common,
	page
}
