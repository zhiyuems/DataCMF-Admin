import apiConfig from "@/config/api";

// 系统配置渲染配置
export default {
	parseData: function (res) {
		return {
			data: res[apiConfig.common.data],				//分析行数据字段结构
			msg: res[apiConfig.common.subMsg],			    //分析描述字段结构
			code: res[apiConfig.common.code]				//分析状态字段结构
		}
	},
	props: {
		label: 'variable_label',					//映射label显示字段
		value: 'variable_value',					//映射value值字段
		group: 'group',				          	//映射group值字段
		prop: 'variable_name',					//映射prop值字段
		describe: 'variable_describe',					//映射describe值字段
		tip: 'variable_tip',					//映射tip值字段
		isSys: 'is_systemvar',					//映射describe值字段
	},
	formType: {
		textarea: ['site_analytics']
	},
	group: [
		{label:'扩展配置', value:'extend'},
	]
}
