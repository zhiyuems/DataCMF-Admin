import API from "@/api";
import ApiConfig from "@/config/api"

//上传配置

export default {
	apiObj: API.common.upload,			//上传请求API对象
	successCode: ApiConfig.common.successCode,					//请求完成代码
	maxSize: 10,						//最大文件大小 默认10MB
	parseData: function (res) {
		return {
			code: res[ApiConfig.common.code],				//分析状态字段结构
			src: res[ApiConfig.common.data]['file_url'],			//分析图片远程地址结构
			msg: res[ApiConfig.common.msg]			//分析描述字段结构
		}
	}
}
