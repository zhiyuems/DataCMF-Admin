/**
 * 路由(菜单)对象配置
 *
 * @link https://lolicode.gitee.io/scui-doc/guide/router.html#%E8%B7%AF%E7%94%B1%E5%AF%B9%E8%B1%A1
 */
const menu = {

	// 路由标识字段
	name: 'name',

	// 路由在游览器地址栏里的hash值字段
	path: 'path',

	// 加载的页面组件位置字段
	component: 'component',

	// 菜单子集字段
	children: 'children',

	// 重定向地址字段
	redirect: 'redirect',

	// Meta集(元数据)字段
	meta: 'meta',

	// 菜单显示名称字段，展示在菜单，标签和面包屑等中
	metaTitle: 'title',

	// 菜单隐藏字段，大部分用在无需显示在左侧菜单中的页面，比如详情页
	metaHidden: 'hidden',

	// 是否固定字段， 类似首页控制台在标签中是没有关闭按钮的
	metaAffix: 'affix',

	// 显示图标字段,建议2级菜单都设置图标，否则菜单折叠都将显示空白
	metaIcon: 'icon',

	// 菜单类型字段： catalogue目录 menu菜单 iframe内嵌页面 link外链 button按钮
	metaType: 'type',

	// 是否隐藏面包屑字段
	metaHiddenBreadcrumb: 'hiddenBreadcrumb',

	// 高亮左侧菜单的路由地址，比如打开详情页需要高亮列表页的菜单
	metaActive: 'active',

	// 颜色值字段
	metaColor: 'color',

	// 是否整页打开路由（脱离框架系），例如：fullpage: true
	metaFullpage: 'fullpage',

	// 静态路由时，所能访问路由的角色，例如：role: ["SA"]
	metaRole: 'role',

	value : {
		// 菜单隐藏的值
		hidden: 0
	},

};

export default menu;
