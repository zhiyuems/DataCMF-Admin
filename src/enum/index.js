/**
 * @description 自动import导入所有 Enum 模块
 */

const files = require.context('./module', false, /\.js$/)
const modules = {}
files.keys().forEach((key) => {
	modules[key.replace(/(\.\/|\.js)/g, '')] = files(key).default
})

export default modules
