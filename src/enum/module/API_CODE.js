
export default {
	SUCCESS: 10000,

	// 服务暂不可用（业务系统不可用）
	SERVICE_UNAVAILABLE: 20000,

	// 业务处理失败
	BUSINESS_PROCESSING_FAILED: 40004
}
