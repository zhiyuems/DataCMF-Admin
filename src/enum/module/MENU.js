
export default {
	// 菜单类型字段： catalogue目录 menu菜单 iframe内嵌页面 link外链 button按钮
	TYPE_CATALOGUE: 'catalogue',
	TYPE_MENU: 'menu',
	TYPE_IFRAME: 'iframe',
	TYPE_LINK: 'link',
	TYPE_BUTTON: 'button',

	// 菜单隐藏，大部分用在无需显示在左侧菜单中的页面，比如详情页
	HIDDEN_SHOW: 1,
	HIDDEN_HIDE: 0,

	// 菜单固定，类似首页控制台在标签中是没有关闭按钮的
	AFFIX_YES: 1,
	AFFIX_NO: 0,

	// 是否隐藏面包屑
	BREADCRUMB_SHOW: 1,
	BREADCRUMB_HIDE: 0,

	// 是否整页打开路由（脱离框架系）
	FULL_PAGE_YES: true,
	FULL_PAGE_NO: false,
}
